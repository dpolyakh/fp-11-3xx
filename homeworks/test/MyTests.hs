-- Your tests go here

module MyTests where

import Test.Tasty (TestTree(..), testGroup)
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Lib

myTests :: [TestTree]
myTests = [ testGroup "HW0"
            [ testCase "Works on Alice" $ hw0_0 "Alice" @?= "Hello, Alice" ]
            ,
          	testGroup "Termpaper myEval1" [
                       testCase "myEval1 (\\x . (\\x . x) x) = (\\x . (\\x . x) x)" $ myEval1 (Lambda "x" (Apply (Lambda "x" (Variable "x")) (Variable "x"))) @?= (Lambda "x" (Apply (Lambda "x" (Variable "x")) (Variable "x"))),
                       testCase "myEval1 (\\x . (\\x . x)) x = (\\x . x)" $ myEval1 (Apply (Lambda "x" (Lambda "x" (Variable "x"))) (Variable "x")) @?= (Lambda "x" (Variable "x")),
                       testCase "myEval1 (\\x . (\\y . x y) (\\z . z) (\\u . \\v . v)) == (\\y . (\\z . z) y) (\\u . (\\v . v))" $ myEval1 (Apply (Apply (Lambda "x" (Lambda "y" (Apply (Variable "x") (Variable "y")))) (Lambda "z" (Variable "z"))) (Lambda "u" (Lambda "v" (Variable "v")))) @?= (Apply (Lambda "y" (Apply (Lambda "z" (Variable "z")) (Variable "y"))) (Lambda "u" (Lambda "v" (Variable "v"))))
                 ],
               testGroup "Termpaper myEval" [
                     testCase "myEval (\\x . (\\x . x) x) = (\\x . (\\x . x) x)" $ myEval (Lambda "x" (Apply (Lambda "x" (Variable "x")) (Variable "x"))) @?= (Lambda "x" (Apply (Lambda "x" (Variable "x")) (Variable "x"))),
                     testCase "myEval (\\x . (\\x . x)) x = (\\x . x)" $ myEval (Apply (Lambda "x" (Lambda "x" (Variable "x"))) (Variable "x")) @?= (Lambda "x" (Variable "x")),
                     testCase "myEval (\\x . (\\y . x y) (\\z . z) (\\u . \\v . v)) == (\\y . (\\z . z) y) (\\u . (\\v . v))" $ myEval (Apply (Apply (Lambda "x" (Lambda "y" (Apply (Variable "x") (Variable "y")))) (Lambda "z" (Variable "z"))) (Lambda "u" (Lambda "v" (Variable "v")))) @?= (Lambda "u" (Lambda "v" (Variable "v")))
               ]
            ]

            