-- Срок: 2016-03-22 (100%), 2016-03-29 (50%)

module HW3
       ( foldr'
       , groupBy'
       , Either' (..)
       , either'
       , lefts'
       , rights'
       ) where

-- foldr' - правая свёртка
-- foldr' + b0 [an,...,a1,a0] =
--    = (an + ... + (a1 + (a0 + b0)))
foldr' :: (a -> b -> b) -> b -> [a] -> b
foldr' f b [] = b
foldr' f b (a:as) = f a (foldr' f b as)

-- Группировка
-- > groupBy' (==) [1,2,2,2,1,1,3]
-- [[1],[2,2,2],[1,1],[3]]
groupBy' :: (a -> a -> Bool) -> [a] -> [[a]]
groupBy' _ [] = []
groupBy' cmp (x:xs) = (x:ys) : groupBy' cmp zs
	where (ys,zs) = span (cmp x) xs

-- Сумма типов a и b
data Either' a b = Left' a
                 | Right' b
                 deriving (Show)
-- Например, Either String b -
-- может быть результат типа b
-- или ошибка типа String

-- Из функций (a -> c) и (b -> c) получить функцию
-- над суммой a и b
-- Например,
-- > either' length id (Left' [1,2,3])
-- 3
-- > either' length id (Right' 4)
-- 4
-- > :t either' length id
-- Either [Int] Int -> Int
either' :: (a -> c) -> (b -> c) -> Either' a b -> c
either' f g (Left' l) = f l
either' f g (Right' r) = g r

-- Получение значений соответствующих слагаемых
lefts' :: [Either' a b] -> [a]
lefts' [] = []
lefts' (Left' l:mas) = l : lefts' mas
lefts' (Right' r:mas) = lefts' mas

rights' :: [Either' a b] -> [b]
rights' [] = []
rights' (Left' l:mas) = rights' mas
rights' (Right' r:mas) = r : rights' mas

