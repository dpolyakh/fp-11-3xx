module Termpaper where

data MyTerm = Variable String
          | Lambda String MyTerm
          | Apply MyTerm MyTerm
          deriving Eq

instance Show MyTerm where
    show (Variable x) = x
    show (Lambda variable term) = "(\\" ++ variable ++ "." ++ (show term) ++ ")" 
    show (Apply term1 term2) = (show term1) ++ " " ++ (show term2)

renameVariable :: [String] -> String -> Int -> String
renameVariable list x n | elem (x ++ (show n)) list = renameVariable list x (n+1)
                        | otherwise = x ++ (show n)

renameFreeVariables :: [String] -> MyTerm -> MyTerm
renameFreeVariables lst (Lambda x term) | elem x lst = Lambda x (renameFreeVariables (filter (\el -> el /= x) lst) term)
                                        | otherwise = Lambda x (renameFreeVariables lst term)
renameFreeVariables lst (Apply t1 t2) = Apply (renameFreeVariables lst t1) (renameFreeVariables lst t2)
renameFreeVariables lst (Variable x) | elem x lst = Variable (renameVariable lst x 0)
                                     | otherwise = Variable x

setVariable :: String -> [String] -> MyTerm -> MyTerm -> MyTerm
setVariable var lst (Lambda x term) needTerm | x /= var = Lambda x (setVariable var (x:lst) term needTerm)
                                             | otherwise = Lambda x term
setVariable var lst (Apply term1 term2) needTerm = Apply (setVariable var lst term1 needTerm) (setVariable var lst term2 needTerm)
setVariable var lst (Variable x) needTerm | x == var = renameFreeVariables lst needTerm
                                          | otherwise = Variable x  

-- One step term calculating
myEval1 :: MyTerm -> MyTerm
myEval1 (Apply (Lambda variable term) t) = setVariable variable [] term t
myEval1 (Apply (Apply term1 term2) t) = Apply (myEval1 (Apply term1 term2)) t
myEval1 term = term

-- Full term calculating
myEval :: MyTerm -> MyTerm
myEval (Apply (Lambda variable term) t) = myEval (setVariable variable [] term t)
myEval (Apply (Apply term1 term2) t) = myEval (Apply (myEval (Apply term1 term2)) t)
myEval term = term